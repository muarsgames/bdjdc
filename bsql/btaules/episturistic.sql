CREATE TABLE pisturistic
(
	expedient	CHAR(12),
	barri		TINYINT(1),
	tipus_carrer	ENUM
			(
				"AV", "BDA", "C",
				"CSTA", "CTRA", "G.V.",
				"PG", "PL", "PLA", "PTGE",
				"RBLA", "RDA", "RIER",
				"TRAV", "TRVS"
			)
			,
	carrer		CHAR(50) NOT NULL,
	tipus_num	BOOLEAN NOT NULL,
	num1		SMALLINT NOT NULL,
	lletra1		CHAR DEFAULT NULL,
	num2		SMALLINT DEFAULT NULL,
	lletra2		CHAR DEFAULT NULL,
	bloc		CHAR(2) DEFAULT NULL, # CHAR??
	portal		CHAR(2) DEFAULT NULL, # CHAR??
	escala		CHAR(2) DEFAULT NULL, # CHAR??
	pis		CHAR(2) DEFAULT NULL, # NOT NULL?
	porta		TINYINT DEFAULT NULL, # NOT NULL?

	CONSTRAINT FK_pisturistic_barri FOREIGN KEY (barri) REFERENCES barri (codi),
	CONSTRAINT PK_pisturistic PRIMARY KEY (expedient)
)
;
