CREATE TABLE lloguerpreu
(
	barri		TINYINT(1),
	any		SMALLINT NOT NULL,
	trimestre1	FLOAT(5,1) DEFAULT NULL,
	trimestre2	FLOAT(5,1) DEFAULT NULL,
	trimestre3	FLOAT(5,1) DEFAULT NULL,
	trimestre4	FLOAT(5,1) DEFAULT NULL,

	CONSTRAINT FK_lloguerpreu_barri FOREIGN KEY (barri) REFERENCES barri (codi),
	CONSTRAINT PK_lloguerpreu PRIMARY KEY (barri, any)
)
;

# DUBTES:
# taula lloguer i taula superficie o taula habitatge amb camps per lloguer i per superfície?

