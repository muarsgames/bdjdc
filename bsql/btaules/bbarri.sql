CREATE TABLE barri 
(
	codi		TINYINT(1) AUTO_INCREMENT,
	nom		CHAR(50) NOT NULL,
	districte	TINYINT(1),

	CONSTRAINT PK_barri PRIMARY KEY (codi),
	CONSTRAINT UK_barri UNIQUE KEY (nom),
	CONSTRAINT FK_barri_districte FOREIGN KEY (districte) REFERENCES districte (codi)
)
;
