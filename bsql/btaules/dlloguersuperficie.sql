CREATE TABLE lloguersuperficie
(
	barri		TINYINT(1),
	any		SMALLINT NOT NULL,
	trimestre1	FLOAT(4,1) DEFAULT NULL,
	trimestre2	FLOAT(4,1) DEFAULT NULL,
	trimestre3	FLOAT(4,1) DEFAULT NULL,
	trimestre4	FLOAT(4,1) DEFAULT NULL,

	CONSTRAINT FK_lloguersuperficie_barri FOREIGN KEY (barri) REFERENCES barri (codi),
	CONSTRAINT PK_lloguersuperficie PRIMARY KEY (barri, any)
)
;

# DUBTES:
# taula lloguer i taula superficie o taula habitatge amb camps per lloguer i per superfície?

