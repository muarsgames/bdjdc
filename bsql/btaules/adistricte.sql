CREATE TABLE districte
(
	codi		TINYINT(1) AUTO_INCREMENT,
	nom		CHAR(50) NOT NULL,

	CONSTRAINT PK_districte PRIMARY KEY (codi),
	CONSTRAINT UK_districte UNIQUE KEY (nom)
)
;
