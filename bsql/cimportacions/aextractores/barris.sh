#/bin/sh

# bé:
# tail -n +3 2016_LLOGUER_MITJA_MENSUAL_2016.csv | awk -vOFS=";" -F\" '{print $1, $2}'

# molt béeeeee!:
# el sed posa en majúscula el nom del barri
tail -n +3 ../../../adades/cconjunts_de_dades/lloguerpreu/2016_LLOGUER_MITJA_MENSUAL_2016.csv \
| awk -vOFS=";" -F\" '{print $1, $2}'  \
| awk '{gsub(/,;[0-9]+\.\ /, ";"); print}' \
| sed 's/;./\U&/' \
 > ../bconverses/barris.csv

#gsub(/[^0-9]*/, "", $2); gsub(/,*;,*/, ";");gsub(/;;/, ";");print}' | awk -vOFS=";" -F";" '{print $2, $3, $4, $5}' | awk '{gsub(/n.d./, ""); gsub(/\./, ""); gsub(/,/, "."); print}' > 2016_lloguerpreu.csv
