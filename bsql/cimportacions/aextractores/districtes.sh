#/bin/sh

# tail -n +2 ../huts_comunicacio.csv | awk -F";" '{print $2}' | sort -u | tail -n +2 > ../bconverses/districtes.csv

# potser serveix per a reordenar línies
# cat -n ../bconverses/districtes.csv | sed -e '9{h;d};3{x;g}'

# el següent serveix per a extreure els noms dels districtes.
# el sed el que fa és ordenar els districtes segons l'ordre creat per l'ajuntament al 1984!
tail -n +2 ../../../adades/cconjunts_de_dades/pisturistic/huts_comunicacio.csv \
| awk -F";" '{print $2}' | sort -u | tail -n +2 | \
sed -e '8{h;d};10{p;g}' | \
sed -e '7{h;d};9{p;g}' | \
sed -e '6{h;d};8{p;g}' | \
sed -e '4{h;d};7{p;g}' | \
sed -e '3{h;d};6{p;g}' | \
sed -e '3{h;d};4{p;g}'   \
> ../bconverses/districtes.csv

# districtes.csv després s'afegeix a la base de dades mysql, a una taula amb id auto_increment
