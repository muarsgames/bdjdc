#/bin/sh

# no és bonic però funciona! les acumulades (trimestrals) no interessen de moment.
# tail -n +3 ../2016_LLOGUER_MITJA_MENSUAL_2016.csv | awk -vOFS=";" -F\" '{gsub(/[^0-9]*/, "", $2); gsub(/,*;,*/, ";");gsub(/;;/, ";");print}' | awk -vOFS=";" -F";" '{print $2, $3, $4, $5}' | awk '{gsub(/n.d./, ""); gsub(/\./, ""); gsub(/,/, "."); print}' > ../bconverses/2016_lloguerpreu.csv

tail -n +3 ../../../adades/cconjunts_de_dades/lloguerpreu/2016_LLOGUER_MITJA_MENSUAL_2016.csv \
| awk -vOFS=";" -F\" '{gsub(/[^0-9]*/, "", $2); gsub(/,*;,*/, ";");gsub(/;;/, ";");print}' \
| awk -vOFS=";" -F";" '{print $2, $3, $4, $5}' \
| awk '{gsub(/n.d./, ""); gsub(/\./, ""); gsub(/,/, "."); print}' \
> ../bconverses/lloguerpreu2016.csv
