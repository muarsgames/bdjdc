#!/bin/bash

# Base de dades "jdc" per a la IPA de Jocs del Comú

# arsgames.net

# aquest script s'ha d'executar des del directori que
# el conté!

# A partir d´aquí s´executa tot el codi, però primer
# es crea un enllaç per a facilitar la feina:

if [ ! -e ~/jdc ]; then              # Si no existeix "~/jdc"...
	ln -s $(realpath ../../../kcd) ~/jdc  # crea l'enllaç
fi

# el següent és per a crear un sol fitxer sql amb totes les taules,
# actualitzades per si s'hi ha fet un canvi, en alguna:
if [ -e ../../bsql/btaules/totes.sql ]; then
	rm ../../bsql/btaules/totes.sql
fi

cat ../../bsql/btaules/* > ../../bsql/btaules/totes.sql

# el següent fa un source de l'sql que comença la bd:
mysql -uroot -p$(cat ~/mysqljdcrootpasswd.txt) --local-infile=1 < jdc.sql
#PERFER en una versió millor s'hauria de fer:
# sed s/-uroot/-u$USUARIA/ && sed s/-proot/-p$CONTRASSENYA/

rm ../../bsql/btaules/totes.sql
